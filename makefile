all: lib.inc main.asm
	nasm -g lib.inc -felf64 -o lib.o
	nasm -g main.asm -felf64 -o main.o
	ld -o lib lib.o
	ld -o main main.o lib.o

run:
	make
	gdb main -q -ex "layout asm" -ex "layout regs"

clean:
	rm lib.o lib
	rm main.o main

